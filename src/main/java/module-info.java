module church.i18n.response.spring {
  requires static org.jetbrains.annotations;
  requires transitive org.slf4j;
  requires transitive jakarta.servlet;
  //automatic modules 23-10-2023
  requires transitive spring.web;
  requires transitive spring.beans;

  requires church.i18n.response;
  requires church.i18n.processing.exception;
  requires spring.core;

  exports church.i18n.request.filter;
  exports church.i18n.response.filter;
  exports church.i18n.response.handler;
  exports church.i18n.response.status;
  exports church.i18n.response.spring.mapper;

  opens church.i18n.response.handler;
}