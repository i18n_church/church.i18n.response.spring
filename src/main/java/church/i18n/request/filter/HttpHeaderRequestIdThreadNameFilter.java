/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.request.filter;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of filter that checks {@code HttpServletRequest} whether header 'X-Request-ID'
 * is present. If so it set this value as the name of the current thread. When the value is not
 * present it generates random UUID and set its value as current thread name. Allowed characters in
 * X-Request-ID header field are: 0-9A-Za-z_-+=,.;/ and maximal length is 200 characters.
 */
public class HttpHeaderRequestIdThreadNameFilter implements Filter {

  public static final @NotNull String DEFAULT_ESCAPE_REGEX = "[^0-9A-Za-z_\\-+=,.;/]";
  public static final int DEFAULT_MAX_REQUEST_ID_LENGTH = 200;
  public static final @NotNull String DEFAULT_REPLACEMENT_STRING = "+";
  public static final @NotNull String DEFAULT_REQUEST_ID_HEADER_NAME = "X-Request-ID";
  private static final @NotNull Logger log = LoggerFactory
      .getLogger(HttpHeaderRequestIdThreadNameFilter.class);
  private final @NotNull Pattern escapeRegex;
  private final int maxRequestIdLength;
  private final @NotNull String replacementString;
  private final @NotNull String requestIdHeaderName;

  /**
   * Constructor of a filter with default values. Default values are: {@code
   * headerName='X-Request-ID'}, {@code maxRequestIdLength=200} {@code
   * escapeRegex=[^0-9A-Za-z_\-+=,.;/]}, {@code replacementString='+'}. See {@link
   * HttpHeaderRequestIdThreadNameFilter#HttpHeaderRequestIdThreadNameFilter(String, int, String,
   * String)} for more details about parameters and behavior.
   */
  public HttpHeaderRequestIdThreadNameFilter() {
    this(DEFAULT_REQUEST_ID_HEADER_NAME, DEFAULT_MAX_REQUEST_ID_LENGTH, DEFAULT_ESCAPE_REGEX,
        DEFAULT_REPLACEMENT_STRING);
  }

  /**
   * Constructor of a filter with custom class behavior.
   *
   * @param headerName         Header name used to retrieve request ID value. Default:
   *                           X-Request-ID.
   * @param maxRequestIdLength Maximal length of request ID string. Default: max 200 characters.
   * @param escapeRegex        Regex used to escape request ID string. All characters/sequence of
   *                           characters matching this regular expression will be replaced by the
   *                           {@code replacementString}. Default: [^0-9A-Za-z_\-+=,.;/] meaning
   *                           only 0-9A-Za-z_-+=,.;/ characters are allowed. All other characters
   *                           are sanitized.
   * @param replacementString  String used to replace characters matching {@code escapeRegex}.
   *                           Default value is: '+'
   */
  public HttpHeaderRequestIdThreadNameFilter(final @NotNull String headerName,
      final int maxRequestIdLength, final @NotNull String escapeRegex,
      final @NotNull String replacementString) {
    if (maxRequestIdLength < 1) {
      throw new IllegalArgumentException("Max RequestId length should be positive number.");
    }
    this.maxRequestIdLength = maxRequestIdLength;
    this.requestIdHeaderName = Objects.requireNonNull(headerName, "Header name cannot be null.");
    this.escapeRegex = Pattern.compile(Objects.requireNonNull(escapeRegex, "Escape regexp cannot "
        + "be null."));
    this.replacementString = Objects
        .requireNonNull(replacementString, "Replacement string cannot be null.");
  }

  @Override
  public void doFilter(final @NotNull ServletRequest request,
      final @NotNull ServletResponse response, final @NotNull FilterChain chain)
      throws IOException, ServletException {
    String originalName = Thread.currentThread().getName();
    try {
      String requestId = null;
      if (request instanceof HttpServletRequest) {
        requestId = ((HttpServletRequest) request).getHeader(this.requestIdHeaderName);
        //Escape characters. Just in case.
        if (requestId != null) {
          requestId = requestId.substring(0, Math.min(this.maxRequestIdLength, requestId.length()));
          requestId = this.escapeRegex
              .matcher(requestId).replaceAll(this.replacementString);
        }
      }
      if (requestId == null || requestId.isBlank()) {
        requestId = UUID.randomUUID().toString();
      }
      Thread.currentThread().setName(requestId);
      log.debug("Setting up thread name: {}", requestId);
      chain.doFilter(request, response);
    } finally {
      Thread.currentThread().setName(originalName);
    }
  }
}
