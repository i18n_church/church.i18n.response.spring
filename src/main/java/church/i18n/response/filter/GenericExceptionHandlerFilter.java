/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.filter;

import church.i18n.processing.exception.ExceptionUtils;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.response.spring.mapper.ResponseEntityExceptionMapper;
import church.i18n.response.status.HttpStatusCode;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;

public class GenericExceptionHandlerFilter implements Filter, SpringMessageConverterWriter {

  private static final Logger log = LoggerFactory.getLogger(GenericExceptionHandlerFilter.class);

  private final ResponseEntityExceptionMapper<?> mapper;
  private final List<HttpMessageConverter<?>> converters;

  public GenericExceptionHandlerFilter(final ResponseEntityExceptionMapper<?> mapper,
      final List<HttpMessageConverter<?>> converters) {
    this.mapper = mapper;
    this.converters = converters;
  }

  @Override
  public void doFilter(final @NotNull ServletRequest request,
      final @NotNull ServletResponse response,
      final @NotNull FilterChain chain) throws IOException {
    try {
      chain.doFilter(request, response);
    } catch (Exception e) {
      log.error("Unhandled type of exception.", e);
      if (response instanceof HttpServletResponse) {
        ProcessingException responseException = ExceptionUtils.getWrappedException(e)
            .orElse(new ProcessingException("err-g-2", e)
                .withStatus(HttpStatusCode.INTERNAL_SERVER_ERROR_500));
        ResponseEntity<?> responseEntity = this.mapper.error(responseException);

        write((HttpServletRequest) request, (HttpServletResponse) response,
            responseEntity.getStatusCode().value(), Objects.requireNonNull(responseEntity.getBody()));
      } else {
        log.error("Missing response type handler for class: {}",
            response.getClass().getCanonicalName(), e);
      }
    }
  }

  @Override
  public @NotNull List<HttpMessageConverter<?>> getConverters() {
    return this.converters;
  }
}
