/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.filter;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import  org.springframework.http.converter.HttpMessageConverter;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.util.MimeTypeUtils;

public interface SpringMessageConverterWriter {

  /**
   * Serialize the content of {@code object} into {@link jakarta.servlet.ServletResponse} and set the
   * response {@code HTTP status}.
   *
   * @param servletRequest  The {@link jakarta.servlet.ServletRequest} object contains the client's
   *                        request
   * @param servletResponse The {@link jakarta.servlet.ServletResponse} object contains the filter's
   *                        response
   * @param httpStatus      The HTTP status to return.
   * @param object          The object to convert.
   * @throws IOException Thrown when message converter is not able to serialize object.
   */
  default void write(final @NotNull HttpServletRequest servletRequest,
      final @NotNull HttpServletResponse servletResponse, final int httpStatus,
      final @NotNull Object object)
      throws IOException {
    servletResponse.setStatus(httpStatus);
    write(servletRequest, servletResponse, object);
  }

  /**
   * Serialize the content of {@code object} into {@link jakarta.servlet.ServletResponse}.
   *
   * @param servletRequest  The {@link jakarta.servlet.ServletRequest} object contains the client's
   *                        request
   * @param servletResponse The {@link jakarta.servlet.ServletResponse} object contains the filter's
   *                        response
   * @param object          The object to convert.
   * @throws IOException Thrown when message converter is not able to serialize object.
   */
  default void write(final @NotNull HttpServletRequest servletRequest,
      final @NotNull HttpServletResponse servletResponse,
      final @NotNull Object object) throws IOException {
    //Could be of format: text/html, application/xhtml+xml, application/xml;q=0.9, image/webp,
    // */*;q=0.8
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept
    // https://developer.mozilla.org/en-US/docs/Glossary/quality_values
    final List<MediaType> mediaTypes = MediaType
        .parseMediaTypes(servletRequest.getHeader("accept"));
    MimeTypeUtils.sortBySpecificity(mediaTypes);
    for (MediaType mediaType : mediaTypes) {
      for (HttpMessageConverter<?> messageConverter : getConverters()) {
        if (messageConverter.canWrite(object.getClass(), mediaType)) {
          HttpOutputMessage outputMessage = new ServletServerHttpResponse(servletResponse);
          @SuppressWarnings("unchecked")
          final HttpMessageConverter<Object> converter =
              (HttpMessageConverter<Object>) messageConverter;
          converter.write(object, mediaType, outputMessage);
          return;
        }
      }
    }
  }

  @NotNull
  List<HttpMessageConverter<?>> getConverters();
}
