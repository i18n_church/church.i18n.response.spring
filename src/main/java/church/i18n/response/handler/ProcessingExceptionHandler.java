/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import church.i18n.processing.exception.ExceptionUtils;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.status.DefaultStatus;
import church.i18n.response.spring.mapper.ResponseEntityExceptionMapper;
import church.i18n.response.status.HttpStatusCode;
import jakarta.servlet.http.HttpServletRequest;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * Handler catching general {@link ProcessingException} exception.
 *
 * @param <R> Type of the endpoint response.
 */
public interface ProcessingExceptionHandler<R> extends ResponseEntityExceptionMapper<R> {

  @NotNull
  String RESOURCE_BUNDLE_NAME = "church.i18n.response.handler.errors-generic-handler";

  /**
   * General exception handler for {@link ProcessingException} exception.
   *
   * @param exception      Exception that will be handled.
   * @param servletRequest HTTP servlet request container.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(ProcessingException.class)
  default @NotNull ResponseEntity<R> handleProcessingException(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest) {
    if (exception.getStatus() == DefaultStatus.UNKNOWN) {
      //In the case we do not specify the code we return internal server error, usually for security
      // reasons we do not want to specify any details out of our system and logs. For this reason
      // we remap exception by default to see it in the log, but not in the response.
      ProcessingException remappedException = ExceptionUtils
          .wrap(exception, "err-g-1")
          .withStatus(HttpStatusCode.INTERNAL_SERVER_ERROR_500);
      return error(remappedException, servletRequest, webRequest);
    }
    return error(exception, servletRequest, webRequest);
  }
}
