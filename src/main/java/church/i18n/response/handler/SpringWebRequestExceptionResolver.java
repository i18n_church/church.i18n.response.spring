/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ValueType;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.response.spring.mapper.ResponseEntityExceptionMapper;
import church.i18n.response.status.HttpStatusCode;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

/**
 * Handler that overrides default exception handler from the spring framework and mapping exceptions
 * to {@link ProcessingException}.
 *
 * @param <R> Type of the endpoint response.
 */
public interface SpringWebRequestExceptionResolver<R> extends ResponseEntityExceptionMapper<R> {

  /**
   * Resource bundle consisting of public messages.
   */
  @NotNull
  String RESOURCE_BUNDLE_NAME = "church.i18n.response.handler.errors-request-handler";

  /**
   * Exception handler for {@link AsyncRequestTimeoutException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(AsyncRequestTimeoutException.class)
  default @NotNull ResponseEntity<R> handleAsyncRequestTimeoutExceptionException(
      final @NotNull AsyncRequestTimeoutException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {

    ProcessingException processingException = new ProcessingException("err-r-1", exception)
        .withStatus(HttpStatusCode.SERVICE_UNAVAILABLE_503)
        .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link ConversionNotSupportedException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(ConversionNotSupportedException.class)
  default @NotNull ResponseEntity<R> handleConversionNotSupportedExceptionException(
      final @NotNull ConversionNotSupportedException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("err-r-8", exception)
        .withStatus(HttpStatusCode.INTERNAL_SERVER_ERROR_500)
        .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link HttpMediaTypeNotAcceptableException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
  default @NotNull ResponseEntity<R> handleHttpMediaTypeNotAcceptableExceptionException(
      final @NotNull HttpMediaTypeNotAcceptableException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("err-r-4", exception)
        .withStatus(HttpStatusCode.NOT_ACCEPTABLE_406)
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link HttpMediaTypeNotSupportedException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  default @NotNull ResponseEntity<R> handleHttpMediaTypeNotSupportedExceptionException(
      final @NotNull HttpMediaTypeNotSupportedException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingMessage message = ProcessingMessage
        .withMessage("err-r-3", exception.getContentType())
        .addContextInfo(
            ContextInfo.of("contentType",
                new ContextValue(exception.getContentType(), ValueType.STRING))
        )
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .build();
    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.UNSUPPORTED_MEDIA_TYPE_415);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link HttpMessageNotReadableException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(HttpMessageNotReadableException.class)
  default @NotNull ResponseEntity<R> handleHttpMessageNotReadableExceptionException(
      final @NotNull HttpMessageNotReadableException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("err-r-10", exception)
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link HttpMessageNotWritableException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(HttpMessageNotWritableException.class)
  default @NotNull ResponseEntity<R> handleHttpMessageNotWritableExceptionException(
      final @NotNull HttpMessageNotWritableException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("err-r-11", exception)
        .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
        .withStatus(HttpStatusCode.INTERNAL_SERVER_ERROR_500);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link HttpRequestMethodNotSupportedException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  default @NotNull ResponseEntity<R> handleHttpRequestMethodNotSupportedExceptionException(
      final @NotNull HttpRequestMethodNotSupportedException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest) {
    final Set<HttpMethod> supportedHttpMethods = Objects
        .requireNonNullElseGet(exception.getSupportedHttpMethods(), Set::of);
    final TreeSet<HttpMethod> sortedHttpMethods = new TreeSet<>((o1, o2) -> o1.name().compareToIgnoreCase(o2.name()));
    sortedHttpMethods.addAll(supportedHttpMethods);

    ProcessingMessage message = ProcessingMessage
        .withMessage("err-r-2", exception.getMethod())
        .addContextInfo(
            ContextInfo.of("method")
                .withMessage("err-r-2-c", sortedHttpMethods)
                .withContext(exception.getMethod(), ValueType.STRING)
                .withHelp(ContextValue.fromSet(sortedHttpMethods))
                .build())
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .build();

    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.METHOD_NOT_ALLOWED_405);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link MethodArgumentTypeMismatchException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  default @NotNull ResponseEntity<R> handleMethodArgumentTypeMismatchException(
      final @NotNull MethodArgumentTypeMismatchException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {

    String typeName = Optional.ofNullable(exception.getRequiredType())
        .map(Class::getSimpleName)
        .orElse("?");
    String paramName = Objects.requireNonNullElse(exception.getPropertyName(), "?");
    final Object value = exception.getValue();
    ProcessingMessage message = ProcessingMessage
        .withMessage("err-r-13", paramName)
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .addContextInfo(
            ContextInfo.of(paramName)
                .withContext(String.valueOf(value))
                .withHelp(typeName, ValueType.TYPE)
                .withMessage("err-r-13-c", paramName, typeName, value)
                .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
                .build()
        ).build();

    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link MissingPathVariableException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(MissingPathVariableException.class)
  default @NotNull ResponseEntity<R> handleMissingPathVariableException(
      final @NotNull MissingPathVariableException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    String type = exception.getParameter().getParameterType().getSimpleName();
    ProcessingException processingException = new ProcessingException("err-r-5", exception)
        .addContextInfo(
            ContextInfo.of("pathVariable")
                .withContext(exception.getVariableName())
                .withHelp(type, ValueType.TYPE)
                .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
                .withMessage("err-r-5-c", exception.getVariableName(), type)
                .build()
        )
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .withStatus(HttpStatusCode.INTERNAL_SERVER_ERROR_500);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link MissingServletRequestParameterException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(MissingServletRequestParameterException.class)
  default @NotNull ResponseEntity<R> handleMissingServletRequestParameterExceptionException(
      final @NotNull MissingServletRequestParameterException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest) {
    ProcessingMessage message = ProcessingMessage
        .withMessage("err-r-6")
        .addContextInfo(ContextInfo
            .of("parameter")
            .withContext(exception.getParameterName())
            .withHelp(exception.getParameterType())
            .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
            .withMessage("err-r-6-c", exception.getParameterName(), exception.getParameterType())
            .build()
        )
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .build();

    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link MissingServletRequestPartException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(MissingServletRequestPartException.class)
  default @NotNull ResponseEntity<R> handleMissingServletRequestPartExceptionException(
      final @NotNull MissingServletRequestPartException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingMessage message = ProcessingMessage
        .withMessage("err-r-12", exception.getRequestPartName())
        .addContextInfo(
            ContextInfo.of("requestPartName", new ContextValue(exception.getRequestPartName()))
        )
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .build();

    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link ServletRequestBindingException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(ServletRequestBindingException.class)
  default @NotNull ResponseEntity<R> handleServletRequestBindingExceptionException(
      final @NotNull ServletRequestBindingException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("err-r-7", exception)
        .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link TypeMismatchException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(TypeMismatchException.class)
  default @NotNull ResponseEntity<R> handleTypeMismatchException(
      final @NotNull TypeMismatchException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    String typeName = exception.getRequiredType() == null
                      ? null
                      : exception.getRequiredType().getSimpleName();

    ProcessingMessage message = ProcessingMessage
        .withMessage("err-r-9", exception.getPropertyName())
        .addContextInfo(
            ContextInfo.of(Objects.requireNonNullElse(exception.getPropertyName(), "unknown"))
                .withContext(String.valueOf(exception.getValue()))
                .withHelp(typeName)
                .withMessage("err-r-9-c", exception.getPropertyName(), typeName,
                    exception.getValue())
                .build()
        )
        .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
        .build();

    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

}
