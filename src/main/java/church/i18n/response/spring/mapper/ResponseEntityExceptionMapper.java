/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.mapper;

import church.i18n.processing.exception.ExceptionUtils;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ProcessingMessage;
import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.context.request.WebRequest;

/**
 * Interface for mapper handling data and response messages into user response.
 *
 * @param <R> Type of the user response.
 */
public interface ResponseEntityExceptionMapper<R> extends ResponseMapperProvider {

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull ProcessingMessage... messages) {
    return error(exception, List.of(), messages);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull List<Locale> locales, final @NotNull ProcessingMessage... messages) {
    HttpStatus httpStatus = HttpStatus.resolve(exception.getStatus().getStatusId());
    if (httpStatus == null) {
      if (getMessageStorage() != null) {
        getMessageStorage().clearStorage();
      }
      ProcessingException remappedException = ExceptionUtils.wrap(exception, "err-g-1", exception);
      return error(remappedException, ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR),
          locales, messages);
    }
    return error(exception, ResponseEntity.status(httpStatus), locales, messages);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull BodyBuilder bodyBuilder, final @NotNull ProcessingMessage... messages) {
    return error(exception, bodyBuilder, List.of(), messages);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull BodyBuilder bodyBuilder, final @NotNull List<Locale> locales,
      final @NotNull ProcessingMessage... messages) {
    this.getLogMapper().log(exception);
    final Object value = this.getMapper().map(null, exception, locales, messages);
    //noinspection unchecked
    return (ResponseEntity<R>) bodyBuilder.body(value);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull ProcessingMessage... messages) {
    return error(exception, List.of(), messages);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull List<Locale> locales, final @NotNull ProcessingMessage... messages) {
    return error(exception, locales, messages);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull BodyBuilder bodyBuilder, final @NotNull ProcessingMessage... messages) {
    return error(exception, bodyBuilder, List.of(), messages);
  }

  default @NotNull ResponseEntity<R> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull BodyBuilder bodyBuilder, final @NotNull List<Locale> locales,
      final @NotNull ProcessingMessage... messages) {
    return error(exception, bodyBuilder, locales, messages);
  }
}
