/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.mapper;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import jakarta.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

public class AcceptLanguageAwareResponseMapper implements
    ResponseEntityResponseMapper, ResponseEntityResponseDataMapper,
    ResponseEntityExceptionMapper<Response<?>> {

  private final @NotNull BaseResponseMapper mapper;

  public AcceptLanguageAwareResponseMapper(final @NotNull BaseResponseMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public ResponseMapper<Response<?>> getMapper() {
    return this.mapper;
  }

  @Override
  public LogMapper getLogMapper() {
    return mapper.getLogMapper();
  }

  @Override
  public MessageStorage getMessageStorage() {
    return this.mapper.getMessageStorage();
  }

  @Override
  public @NotNull ResponseEntity<Response<?>> error(final @NotNull ProcessingException exception,
      final HttpServletRequest servletRequest, final WebRequest webRequest,
      final @NotNull ProcessingMessage... messages) {
    List<Locale> locales = Collections.list(servletRequest.getLocales());
    return error(exception, locales, messages);
  }

  @Override
  public @NotNull ResponseEntity<Response<?>> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull List<Locale> locales, final @NotNull ProcessingMessage... messages) {
    List<Locale> allLocales = joinLocales(locales, servletRequest.getLocales());
    return error(exception, allLocales, messages);
  }

  @Override
  public @NotNull ResponseEntity<Response<?>> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull ResponseEntity.BodyBuilder bodyBuilder,
      final @NotNull ProcessingMessage... messages) {
    List<Locale> locales = Collections.list(servletRequest.getLocales());
    return error(exception, bodyBuilder, locales, messages);
  }

  @Override
  public @NotNull ResponseEntity<Response<?>> error(final @NotNull ProcessingException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull ResponseEntity.BodyBuilder bodyBuilder, final @NotNull List<Locale> locales,
      final @NotNull ProcessingMessage... messages) {
    List<Locale> allLocales = joinLocales(locales, servletRequest.getLocales());
    return error(exception, bodyBuilder, allLocales, messages);
  }

  @Override
  public @NotNull <T> ResponseEntity<Response<T>> ok(final @Nullable T data,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull ProcessingMessage... messages) {
    List<Locale> locales = Collections.list(servletRequest.getLocales());
    return ok(data, locales, messages);
  }

  @Override
  public @NotNull <T> ResponseEntity<Response<T>> ok(final @Nullable T data,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @Nullable List<Locale> locales, final @NotNull ProcessingMessage... messages) {
    List<Locale> allLocales = joinLocales(locales, servletRequest.getLocales());
    return ok(data, allLocales, messages);
  }

  @Override
  public @NotNull <T> ResponseEntity<Response<T>> ok(final @Nullable T data,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull ResponseEntity.BodyBuilder bodyBuilder,
      final @NotNull ProcessingMessage... messages) {
    List<Locale> locales = Collections.list(servletRequest.getLocales());
    return ok(data, bodyBuilder, locales, messages);
  }

  @Override
  public @NotNull <T> ResponseEntity<Response<T>> ok(final @Nullable T data,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull ResponseEntity.BodyBuilder bodyBuilder, final @Nullable List<Locale> locales,
      final @NotNull ProcessingMessage... messages) {
    List<Locale> allLocales = joinLocales(locales, servletRequest.getLocales());
    return ok(data, bodyBuilder, allLocales, messages);
  }

  private List<Locale> joinLocales(final @Nullable List<Locale> user,
      final @Nullable Enumeration<Locale> request) {
    final List<Locale> result = new ArrayList<>();
    if (user != null) {
      result.addAll(user);
    }
    if (request != null) {
      result.addAll(Collections.list(request));
    }
    return result;
  }
}
