/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.mapper;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.response.domain.Response;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import jakarta.servlet.http.HttpServletRequest;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.context.request.WebRequest;

/**
 * Base interface for mapping responses to the user response.
 */
public interface ResponseEntityResponseDataCollectionReMapper extends ResponseMapperProvider {

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(final @Nullable Collection<T> data,
      final @NotNull Function<T, U> remap, final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, List.of(), messages);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(
      final @Nullable Collection<T> data, final @NotNull Function<T, U> remap,
      final @NotNull List<Locale> locales, final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, ResponseEntity.ok(), locales, messages);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(final @Nullable Collection<T> data,
      final @NotNull Function<T, U> remap, final @NotNull BodyBuilder bodyBuilder,
      final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, bodyBuilder, List.of(), messages);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(
      final @Nullable Collection<T> data, final @NotNull Function<T, U> remap,
      final @NotNull BodyBuilder bodyBuilder, final @NotNull List<Locale> locales,
      final @NotNull ProcessingMessage... messages) {
    final List<U> remappedData = ((data == null) ? Collections.emptyList() : data.stream().map(remap).toList());
    Response<Collection<U>> response = (Response<Collection<U>>) this.getMapper().map(remappedData,
        (ProcessingException) null, locales, messages);
    return bodyBuilder.body(response);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(final @Nullable Collection<T> data,
      final @NotNull Function<T, U> remap, final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest, final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, List.of(), messages);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(
      final @Nullable Collection<T> data, final @NotNull Function<T, U> remap,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull List<Locale> locales, final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, locales, messages);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(final @Nullable Collection<T> data,
      final @NotNull Function<T, U> remap, final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest, final @NotNull BodyBuilder bodyBuilder,
      final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, bodyBuilder, List.of(), messages);
  }

  default @NotNull <T, U> ResponseEntity<Response<Collection<U>>> ok(
      final @Nullable Collection<T> data, final @NotNull Function<T, U> remap,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest,
      final @NotNull BodyBuilder bodyBuilder, final @NotNull List<Locale> locales,
      final @NotNull ProcessingMessage... messages) {
    return ok(data, remap, bodyBuilder, locales, messages);
  }

}
