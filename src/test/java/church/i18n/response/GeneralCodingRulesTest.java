/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response;

import static com.tngtech.archunit.library.DependencyRules.NO_CLASSES_SHOULD_DEPEND_UPPER_PACKAGES;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.dependencies.SliceRule;

@AnalyzeClasses(packages = "church.i18n", importOptions = ImportOption.DoNotIncludeTests.class)
public class GeneralCodingRulesTest {

  @ArchTest
  private final SliceRule noCycles = slices()
      .matching("church.i18n.(*)..")
      .should()
      .beFreeOfCycles();

  @ArchTest
  private final ArchRule noUpperDependency = NO_CLASSES_SHOULD_DEPEND_UPPER_PACKAGES;
  @ArchTest
  private final ArchRule noStdIo = NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;
  @ArchTest
  private final ArchRule noGenericException = NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;
  @ArchTest
  private final ArchRule noFieldInjection = NO_CLASSES_SHOULD_USE_FIELD_INJECTION;
  @ArchTest
  private final ArchRule noJavaLogging = NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING;
  @ArchTest
  private final ArchRule noJoda = NO_CLASSES_SHOULD_USE_JODATIME;
}
