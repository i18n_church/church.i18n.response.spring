/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.mapper;

import static church.i18n.processing.status.ClientError.GONE;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import church.i18n.response.spring.mapper.AcceptLanguageAwareResponseMapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

class AcceptLanguageAwareResponseMapperTest {

  private static final ProcessingMessage[] NO_MESSAGES = new ProcessingMessage[0];
  private static final ProcessingMessage[] ONE_MESSAGE = {ProcessingMessage.withMessage("err-code")
      .build()};
  private static final ProcessingMessage[] TWO_MESSAGES = {
      ProcessingMessage.withMessage("err-1").build(),
      ProcessingMessage.withMessage("err-2").build()
  };
  private static final List<Locale> NO_LOCALE = List.of();
  private static final List<Locale> ONE_LOCALE = List.of(Locale.TRADITIONAL_CHINESE);
  private static final List<Locale> TWO_LOCALES = List.of(Locale.JAPAN, Locale.ROOT);
  private static final List<Locale> ADD_LOCALES = List.of(Locale.FRANCE, Locale.KOREA);
  private final HttpServletRequest servletRequest = mock(HttpServletRequest.class);
  private final WebRequest webRequest = mock(WebRequest.class);

  private final MessageStorage threadLocal = new ThreadLocalStorage(() -> "main");
  private final BaseResponseMapper responseMapper = spy(BaseResponseMapper.builder().build());
  private final AcceptLanguageAwareResponseMapper dataMapper =
      new AcceptLanguageAwareResponseMapper(responseMapper) {
        @Override
        public ResponseMapper<Response<?>> getMapper() {
          return responseMapper;
        }

        @Override
        public MessageStorage getMessageStorage() {
          return threadLocal;
        }
      };

  @BeforeEach
  void setUp() {
    when(servletRequest.getLocales()).thenReturn(Collections.enumeration(ADD_LOCALES));
  }

  public static Stream<Arguments> dataMessages() {
    return Stream.of(
        Arguments.of("String", NO_MESSAGES),
        Arguments.of(null, ONE_MESSAGE),
        Arguments.of(9.56, TWO_MESSAGES)
    );
  }

  public static Stream<Arguments> dataLocalesMessages() {
    return Stream.of(
        Arguments.of("String", NO_LOCALE, NO_MESSAGES),
        Arguments.of(null, ONE_LOCALE, ONE_MESSAGE),
        Arguments.of(9.56, TWO_LOCALES, TWO_MESSAGES)
    );
  }

  public static Stream<Arguments> exMessages() {
    return Stream.of(
        Arguments.of(new ProcessingException("ex-zero").withStatus(GONE), NO_MESSAGES),
        Arguments.of(new ProcessingException("ex-one").withStatus(GONE), ONE_MESSAGE),
        Arguments.of(new ProcessingException("ex-two").withStatus(GONE), TWO_MESSAGES)
    );
  }

  public static Stream<Arguments> exLocalesMessages() {
    return Stream.of(
        Arguments.of(new ProcessingException("ex-zero").withStatus(GONE), NO_LOCALE, NO_MESSAGES),
        Arguments.of(new ProcessingException("ex-one").withStatus(GONE), ONE_LOCALE, ONE_MESSAGE),
        Arguments.of(new ProcessingException("ex-two").withStatus(GONE), TWO_LOCALES, TWO_MESSAGES)
    );
  }

  public static Stream<Arguments> exLocalesMessagesNoHttpStatus() {
    return Stream.of(
        Arguments.of(new ProcessingException("ex-zero"), NO_LOCALE, NO_MESSAGES),
        Arguments.of(new ProcessingException("ex-one"), ONE_LOCALE, ONE_MESSAGE),
        Arguments.of(new ProcessingException("ex-two"), TWO_LOCALES, TWO_MESSAGES)
    );
  }

  @ParameterizedTest
  @MethodSource("dataMessages")
  void ok_data_rr_messages(final Object data, final ProcessingMessage[] messages) {
    this.dataMapper.ok(data, this.servletRequest, this.webRequest, messages);
    verify(this.responseMapper, times(1)).map(data, (ProcessingException) null, ADD_LOCALES,
        messages);
  }

  @ParameterizedTest
  @MethodSource("dataLocalesMessages")
  void ok_data_rr_locales_messages(final Object data, final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.dataMapper.ok(data, this.servletRequest, this.webRequest, locales, messages);
    verify(this.responseMapper, times(1)).map(data, (ProcessingException) null, joinAdd(locales),
        messages);
  }

  @ParameterizedTest
  @MethodSource("dataLocalesMessages")
  void ok_data_rr_bb_locales_messages(final Object data, final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.dataMapper.ok(data, this.servletRequest, this.webRequest, ResponseEntity.ok(), locales,
        messages);
    verify(this.responseMapper, times(1)).map(data, (ProcessingException) null, joinAdd(locales),
        messages);
  }

  @ParameterizedTest
  @MethodSource("dataMessages")
  void ok_data_rr_bb_messages(final Object data, final ProcessingMessage[] messages) {
    this.dataMapper.ok(data, this.servletRequest, this.webRequest, ResponseEntity.ok(), messages);
    verify(this.responseMapper, times(1)).map(data, (ProcessingException) null, ADD_LOCALES,
        messages);
  }

  @ParameterizedTest
  @MethodSource("exLocalesMessages")
  void test_error_ex_rr_bb_locales_messages(final ProcessingException ex,
      final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.dataMapper.error(ex, this.servletRequest, this.webRequest, ResponseEntity.ok(), locales,
        messages);
    verify(this.responseMapper, times(1)).map(null, ex, joinAdd(locales), messages);
  }

  @ParameterizedTest
  @MethodSource("exLocalesMessages")
  void test_error_ex_rr_locales_messages(final ProcessingException ex, final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.dataMapper.error(ex, this.servletRequest, this.webRequest, locales, messages);
    verify(this.responseMapper, times(1)).map(null, ex, joinAdd(locales), messages);
  }

  @ParameterizedTest
  @MethodSource("exMessages")
  void test_error_ex_rr_bb_messages(final ProcessingException ex,
      final ProcessingMessage[] messages) {
    this.dataMapper.error(ex, this.servletRequest, this.webRequest, ResponseEntity.ok(), messages);
    verify(this.responseMapper, times(1)).map(null, ex, ADD_LOCALES, messages);
  }

  @ParameterizedTest
  @MethodSource("exMessages")
  void test_error_ex_rr_messages(final ProcessingException ex, final ProcessingMessage[] messages) {
    this.dataMapper.error(ex, this.servletRequest, this.webRequest, messages);
    verify(this.responseMapper, times(1)).map(null, ex, ADD_LOCALES, messages);
  }

  private List<Locale> joinAdd(final List<Locale> locales) {
    final List<Locale> result = new ArrayList<>(locales);
    result.addAll(ADD_LOCALES);
    return result;
  }
}
