/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.mapper;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.status.ClientError;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.domain.ResponseMessageDto;
import church.i18n.response.mapper.BaseResponseMapper;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

class DefaultResponseConverterTest {

  private static final String REQUEST_ID = "the-same-response";
  private static final String DATA = "data";

  @Test
  void addMessageSources() {
    BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withProcessingIdProvider(() -> REQUEST_ID)
        .withConfig(DefaultProcessingExceptionConfig.builder()
            .withExposeSecurityPolicies(Set.of(PUBLIC, SYSTEM_EXTERNAL, SYSTEM_INTERNAL))
            .build())
        .build();

    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addPreferredLocale(Locale.ENGLISH);

    ProcessingException exception = new ProcessingException("msg-A1")
        .withStatus(ClientError.GONE);

    DefaultResponseConverter responseMapper = new DefaultResponseConverter(mapper);
    ResponseEntity<Response<?>> mappedData = responseMapper.error(exception);

    ResponseEntity<Response<Object>> expected = new ResponseEntity<>(
        new ResponseDto<>(REQUEST_ID, null, new ResponseMessageDto("msg-A1", "msg-A1",
            "ERROR", null, List.of()), List.of()), HttpStatus.GONE);

    assertEquals(expected, mappedData);
  }

  @Test
  void mapper_noStatusMessageNoMessagesReturned() {
    ProcessingIdProvider idProvider = () -> "pid";
    ThreadLocalStorage storage = new ThreadLocalStorage(idProvider);
    MockHttpServletRequest request = new MockHttpServletRequest();

    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle("i18n.A"))
        .withProcessingIdProvider(idProvider)
        .withMessageStorage(storage)
        .withConfig(config)
        .withLogMapper(new MessageTypeLogMapper(logSanitizer, config))
        .withExposeSanitizer(new DefaultSecurityInfoSanitizer(
            ProcessingExceptionConfig::defaultSecurityPolicy,
            ProcessingExceptionConfig::exposeSecurityPolicies))
        .build();

    storage.addMessages(idProvider.getProcessingId(), new ProcessingMessage("err-a-b"));

    DefaultResponseConverter responseMapper = new DefaultResponseConverter(mapper);
    ResponseEntity<Response<?>> response = responseMapper.error(
        new ProcessingException("error-without-http-status"));

    ResponseEntity<Response<Object>> expected = new ResponseEntity<>(
        new ResponseDto<>(idProvider.getProcessingId(), null,
            new ResponseMessageDto("err-g-1", "Internal Server Error.",
                "ERROR", null, List.of()), List.of()),
        HttpStatus.INTERNAL_SERVER_ERROR);

    assertEquals(expected, response);
  }

  @Test
  void mapper_dataOnly() {
    ProcessingIdProvider idProvider = () -> "pid";
    MockHttpServletRequest request = new MockHttpServletRequest();

    BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withProcessingIdProvider(idProvider)
        .withBundle(new PolyglotMultiResourceBundle("i18n.A"))
        .build();
    DefaultResponseConverter responseMapper = new DefaultResponseConverter(mapper);

    ResponseEntity<Response<String>> response = responseMapper.ok("data");
    ResponseEntity<Response<String>> expected =
        ResponseEntity.ok(new ResponseDto<>(idProvider.getProcessingId(), "data", null, List.of()));

    assertEquals(expected, response);
  }

  @Test
  void mapper_dataWithMessages() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.ENGLISH);
      ProcessingIdProvider idProvider = () -> "pid";
      ThreadLocalStorage storage = new ThreadLocalStorage(idProvider);
      MockHttpServletRequest request = new MockHttpServletRequest();

      ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
          .withExposeSecurityPolicies(Set.of(SYSTEM_EXTERNAL, SYSTEM_INTERNAL, PUBLIC))
          .build();
      SecurityInfoSanitizer exposeSanitizer = new DefaultSecurityInfoSanitizer(
          ProcessingExceptionConfig::defaultSecurityPolicy,
          ProcessingExceptionConfig::exposeSecurityPolicies);
      BaseResponseMapper mapper = BaseResponseMapper.builder()
          .withBundle(new PolyglotMultiResourceBundle("i18n.A"))
          .withConfig(config)
          .withProcessingIdProvider(idProvider)
          .withExposeSanitizer(exposeSanitizer)
          .withMessageStorage(storage)
          .build();

      storage.addMessages(idProvider.getProcessingId(),
          ProcessingMessage.withMessage("msg").withSecurityLevel(SYSTEM_INTERNAL).build());

      DefaultResponseConverter responseMapper = new DefaultResponseConverter(
          mapper);
      ResponseEntity<Response<String>> response = responseMapper.ok("data");

      ResponseEntity<Response<String>> expected = ResponseEntity.ok(new ResponseDto<>(
          idProvider.getProcessingId(), "data", null,
          List.of(new ResponseMessageDto("msg", "appetizer-english", "ERROR", null, List.of())
          )));

      assertEquals(expected, response);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void mapErrorResponse_noHttpStatus() {
    final BaseResponseMapper mapper = spy(BaseResponseMapper.builder()
        .withProcessingIdProvider(() -> "the-same-response")
        .build());
    final MessageStorage threadLocal = new ThreadLocalStorage(() -> "main");
    DefaultResponseConverter responseMapper = new DefaultResponseConverter(mapper);

    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addPreferredLocale(Locale.ENGLISH);

    ResponseEntity<Response<?>> mappedData = responseMapper
        .error(new ProcessingException("msg-A1"));

  }
}
