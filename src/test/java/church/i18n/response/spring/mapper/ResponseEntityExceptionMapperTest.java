/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.mapper;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static church.i18n.processing.status.ClientError.GONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ExceptionUtils;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

class ResponseEntityExceptionMapperTest {

  private static final ProcessingMessage[] NO_MESSAGES = new ProcessingMessage[0];
  private static final ProcessingMessage[] ONE_MESSAGE = {ProcessingMessage.withMessage("err-code")
      .build()};
  private static final ProcessingMessage[] TWO_MESSAGES = {
      ProcessingMessage.withMessage("err-1").build(),
      ProcessingMessage.withMessage("err-2").build()
  };
  private static final List<Locale> NO_LOCALE = List.of();
  private static final List<Locale> ONE_LOCALE = List.of(Locale.TRADITIONAL_CHINESE);
  private static final List<Locale> TWO_LOCALES = List.of(Locale.JAPAN, Locale.ROOT);
  private final HttpServletRequest servletRequest = mock(HttpServletRequest.class);
  private final WebRequest webRequest = mock(WebRequest.class);

  final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
      .withDefaultMessageType(MessageStatus.ERROR)
      .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
      .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
      .build();
  final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
      ProcessingExceptionConfig::defaultSecurityPolicy,
      ProcessingExceptionConfig::logSecurityPolicies);
  private final BaseResponseMapper responseMapper = spy(BaseResponseMapper.builder().build());
  private final MessageStorage threadLocal = new ThreadLocalStorage(() -> "main");
  private final ResponseEntityExceptionMapper<ResponseEntity<Response<?>>> exMapper =
      new ResponseEntityExceptionMapper<>() {
        @Override
        public ResponseMapper<Response<?>> getMapper() {
          return responseMapper;
        }

        @Override
        public LogMapper getLogMapper() {
          return new MessageTypeLogMapper(logSanitizer, config);
        }

        @Override
        public MessageStorage getMessageStorage() {
          return threadLocal;
        }
      };

  public static Stream<Arguments> exMessages() {
    return Stream.of(
        Arguments.of(new ProcessingException("ex-zero").withStatus(GONE), NO_MESSAGES),
        Arguments.of(new ProcessingException("ex-one").withStatus(GONE), ONE_MESSAGE),
        Arguments.of(new ProcessingException("ex-two").withStatus(GONE), TWO_MESSAGES)
    );
  }

  public static Stream<Arguments> exLocalesMessages() {
    return Stream.of(
        Arguments.of(new ProcessingException("ex-zero").withStatus(GONE), NO_LOCALE, NO_MESSAGES),
        Arguments.of(new ProcessingException("ex-one").withStatus(GONE), ONE_LOCALE, ONE_MESSAGE),
        Arguments.of(new ProcessingException("ex-two").withStatus(GONE), TWO_LOCALES, TWO_MESSAGES)
    );
  }

  public static Stream<Arguments> exLocalesMessagesNoHttpStatus() {
    return Stream.of(
        Arguments.of(new ProcessingException("ex-zero"), NO_LOCALE, NO_MESSAGES),
        Arguments.of(new ProcessingException("ex-one"), ONE_LOCALE, ONE_MESSAGE),
        Arguments.of(new ProcessingException("ex-two"), TWO_LOCALES, TWO_MESSAGES)
    );
  }

  @ParameterizedTest
  @MethodSource("exLocalesMessages")
  void test_error_ex_rr_bb_locales_messages(final ProcessingException ex,
      final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.exMapper.error(ex, this.servletRequest, this.webRequest, ResponseEntity.ok(), locales,
        messages);
    verify(this.responseMapper, times(1)).map(null, ex, locales, messages);
  }

  @ParameterizedTest
  @MethodSource("exLocalesMessages")
  void test_error_ex_rr_locales_messages(final ProcessingException ex, final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.exMapper.error(ex, this.servletRequest, this.webRequest, locales, messages);
    verify(this.responseMapper, times(1)).map(null, ex, locales, messages);
  }

  @ParameterizedTest
  @MethodSource("exMessages")
  void test_error_ex_rr_bb_messages(final ProcessingException ex,
      final ProcessingMessage[] messages) {
    this.exMapper.error(ex, this.servletRequest, this.webRequest, ResponseEntity.ok(), messages);
    verify(this.responseMapper, times(1)).map(null, ex, List.of(), messages);
  }

  @ParameterizedTest
  @MethodSource("exMessages")
  void test_error_ex_rr_messages(final ProcessingException ex, final ProcessingMessage[] messages) {
    this.exMapper.error(ex, this.servletRequest, this.webRequest, messages);
    verify(this.responseMapper, times(1)).map(null, ex, List.of(), messages);
  }

  @ParameterizedTest
  @MethodSource("exLocalesMessages")
  void test_error_ex_bb_locales_messages(final ProcessingException ex, final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.exMapper.error(ex, ResponseEntity.ok(), locales, messages);
    verify(this.responseMapper, times(1)).map(null, ex, locales, messages);
  }

  @ParameterizedTest
  @MethodSource("exLocalesMessages")
  void test_error_ex_locales_messages(final ProcessingException ex, final List<Locale> locales,
      final ProcessingMessage[] messages) {
    this.exMapper.error(ex, locales, messages);
    verify(this.responseMapper, times(1)).map(null, ex, locales, messages);
  }

  @ParameterizedTest
  @MethodSource("exMessages")
  void test_error_ex_bb_messages(final ProcessingException ex, final ProcessingMessage[] messages) {
    this.exMapper.error(ex, ResponseEntity.ok(), messages);
    verify(this.responseMapper, times(1)).map(null, ex, List.of(), messages);
  }

  @ParameterizedTest
  @MethodSource("exMessages")
  void test_error_ex_messages(final ProcessingException ex, final ProcessingMessage[] messages) {
    this.exMapper.error(ex, messages);
    verify(this.responseMapper, times(1)).map(null, ex, List.of(), messages);
  }


  @ParameterizedTest
  @MethodSource("exLocalesMessagesNoHttpStatus")
  void test_error_ex_locales_messages_noHttpStatus(final ProcessingException ex,
      final List<Locale> locales,
      final ProcessingMessage[] messages) {
    final ResponseEntity<ResponseEntity<Response<?>>> response = this.exMapper.error(ex, locales,
        messages);
    //not calling with original exception
    verify(this.responseMapper, times(0)).map(null, ex, locales, messages);
    //calling with remapped exception
    ProcessingException remappedException = ExceptionUtils.wrap(ex, "err-g-1", ex);
    verify(this.responseMapper, times(1)).map(null, remappedException, locales, messages);
    assertEquals(HttpStatusCode.valueOf(500), response.getStatusCode());
  }
}
