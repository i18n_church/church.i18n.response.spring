/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.filter;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.spring.mapper.DefaultResponseConverter;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

class GenericExceptionHandlerFilterTest {

  private DefaultResponseConverter mapper;
  private HttpMessageConverter<ResponseDto<String>> converter;

  @BeforeEach
  void setUp() {
    final Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);

      ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
          .withDefaultMessageType(MessageStatus.ERROR)
          .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
          .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
          .build();
      SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
          ProcessingExceptionConfig::defaultSecurityPolicy,
          ProcessingExceptionConfig::logSecurityPolicies
      );
      final BaseResponseMapper baseResponseMapper = BaseResponseMapper.builder()
          .withBundle(new PolyglotMultiResourceBundle(("i18n.A")))
          .withProcessingIdProvider(() -> "main")
          .withConfig(config)
          .withLogMapper(new MessageTypeLogMapper(logSanitizer, config))
          .withExposeSanitizer(new DefaultSecurityInfoSanitizer(
              ProcessingExceptionConfig::defaultSecurityPolicy,
              ProcessingExceptionConfig::exposeSecurityPolicies))
          .build();
      this.mapper = new DefaultResponseConverter(baseResponseMapper);
      this.converter = new RestResponseDtoHttpMessageConverter();
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void doFilter_generalException() throws Exception {
    MockMvcBuilders.standaloneSetup(new TestController())
        .addFilter(new GenericExceptionHandlerFilter(this.mapper, List.of(this.converter)))
        .addFilter((req, resp, chain) -> {
          throw new UnsupportedOperationException("ExceptionFilter applied!");
        })
        .build()
        .perform(get("/test?userEmail=a@a.aa").accept("application/xml;q=0.9", "image/webp",
            "text/html", "application/xhtml+xml", "*/*;q=0.8"))
        .andExpect(status().isInternalServerError())
        .andExpect(content().string("[requestId='main'|data=null|errorCode=err-g-2|errorSeverity="
            + "ERROR|errorMessage=Internal Server Error - Generic filter.|messagesSize=0]"));
  }
}
