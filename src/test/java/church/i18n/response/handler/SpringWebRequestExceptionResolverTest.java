/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.ContextInfoDto;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import java.beans.PropertyChangeEvent;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.mock.http.MockHttpInputMessage;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.MultipartFile;

class SpringWebRequestExceptionResolverTest {

  private static MockMvc customFilterMockMvc;

  @BeforeAll
  static void prepareAllTests() {
    customFilterMockMvc = MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setControllerAdvice(new TestSpringWebRequestExceptionResolverControllerAdvice())
        .setMessageConverters(new ResponseDtoToStringHttpMessageConverter())
        //.setMessageConverters(new HttpNotReadableObjectToStringHttpMessageConverter())
        .build();
  }

  @Test
  void throwMissingPathVariableException() throws Exception {
    //endpoint throws exception that is specified in the request parameter.
    final String response = customFilterMockMvc
        .perform(MockMvcRequestBuilders
            .get("/missing-path-variable/ /asdf"))
        .andReturn()
        .getResponse()
        .getContentAsString();

    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
            + "code='err-r-5', message='Request path variable is missing in URL.', type=ERROR, "
            + "helpUri='null', context=[ContextInfoDto{name='pathVariable', value=id, "
            + "valueType='null', help=Long, helpType='type', message='Request path variable "
            + "\"id\" of type \"Long\" is missing.'}]}, messages=[]}",
        response);
  }

  @Test
  void testAsyncRequestTimeoutException() throws Exception {
    //endpoint throws exception that is specified in the request parameter.
    final String response = customFilterMockMvc
        .perform(MockMvcRequestBuilders
            .get("/throwNotAliveExceptions")
            .param("name", "AsyncRequestTimeoutException"))
        .andReturn()
        .getResponse()
        .getContentAsString();

    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
        + "code='err-r-1', message='Asynchronous request has timed-out.', type=ERROR, "
        + "helpUri='null', context=[]}, messages=[]}", response);
  }

  @Test
  void testHttpMessageNotWritableException() throws Exception {
    //endpoint throws exception that is specified in the request parameter.
    MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setControllerAdvice(new TestSpringWebRequestExceptionResolverControllerAdvice())
        .setMessageConverters(
            new ResponseDtoToStringHttpMessageConverter(),
            new HttpNotRwObjectToStringHttpMessageConverter()
        )
        .build();

    final String response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.get("/http-message-not-writable-exception"))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
            + "code='err-r-11', message='Message converter failed to write output stream.', "
            + "type=ERROR, helpUri='null', context=[]}, messages=[]}",
        response);
  }

  @Test
  void testHttpMessageNotReadableException() throws Exception {
    //endpoint throws exception that is specified in the request parameter.
    MockMvc httpMessageNotReadableMockMvc = MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setControllerAdvice(new TestSpringWebRequestExceptionResolverControllerAdvice())
        .setMessageConverters(
            new ResponseDtoToStringHttpMessageConverter(),
            new HttpNotRwObjectToStringHttpMessageConverter()
        )
        .build();

    String invalidJsonPayload = "{invalid_json}";
    var response = httpMessageNotReadableMockMvc
        .perform(MockMvcRequestBuilders
            .post("/http-message-not-readable-exception")
            .content(invalidJsonPayload)
            .contentType(MediaType.APPLICATION_JSON).characterEncoding(StandardCharsets.UTF_8)
            .accept(MediaType.TEXT_HTML)
        )
        .andReturn()
        .getResponse();//where is exception consumed and converted to 400?
    assertNull(response.getErrorMessage());
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
            + "code='err-r-10', message='Message converter failed to read input stream.', "
            + "type=ERROR, helpUri='null', context=[]}, messages=[]}",
        response.getContentAsString());
  }

  @Test
  void testServletRequestBindingException() throws Exception {
    //endpoint throws exception that is specified in the request parameter.
    final String response = customFilterMockMvc
        .perform(MockMvcRequestBuilders
            .get("/servlet-request-binding-exception"))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
        + "code='err-r-7', message='Unable to bind request.', type=ERROR, helpUri='null', "
        + "context=[]}, messages=[]}", response);
  }

  @Test
  void handleConversionNotSupportedException() throws Exception {
    final MockHttpServletResponse response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.delete("/converter").param("id", "99"))
        .andReturn()
        .getResponse();
    assertEquals(
        "ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
            + "code='err-r-8', message='No suitable converter found.', type=ERROR, "
            + "helpUri='null', context=[]}, messages=[]}",
        response.getContentAsString()
    );
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
  }

  @Test
  void handleHttpMediaTypeNotSupported() throws Exception {
    final MockHttpServletResponse response = customFilterMockMvc
        .perform(MockMvcRequestBuilders
            .get("/mediaType")
            .header("Content-type", MediaType.APPLICATION_XML))
        .andReturn()
        .getResponse();
    assertEquals("ResponseDto{requestId='main', data=null, "
            + "error=ResponseMessageDto{code='err-r-3', "
            + "message='Request handler does not support \"application/xml\" content type.', "
            + "type=ERROR, helpUri='null', context=[ContextInfoDto{name='contentType', "
            + "value=application/xml, valueType='string', help=null, helpType='null', "
            + "message='null'}]}, messages=[]}",
        response.getContentAsString());
    assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), response.getStatus());
  }

  @Test
  void handleMediaTypeNotAcceptable_noFailover() throws Exception {
    MockMvc mediaConverterNotFoundMockMvc = MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setMessageConverters(new ResponseDtoToStringHttpMessageConverter())
        .setControllerAdvice(
            new TestSpringWebRequestExceptionResolverControllerAdvice(),
            new ResponseDtoToStringHttpMessageConverter()
        )
        .build();
    final MockHttpServletResponse response = mediaConverterNotFoundMockMvc
        .perform(MockMvcRequestBuilders
            .get("/mediaTypeNotAcceptable")
            .accept(MediaType.APPLICATION_PDF))
        .andReturn()
        .getResponse();
    assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), response.getStatus());
    //requires only PDF, we are not able to produce
    //As per standard, there no content, not even default one is provided.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/406
    // "the server is unwilling to supply a default representation."
    assertTrue(response.getContentAsString().isEmpty());
    assertEquals("text/html", response.getHeader("Accept"));
  }

  @Test
  void handleMethodArgumentTypeMismatchException() throws Exception {
    final MockHttpServletResponse response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.delete("/method/asd").param("id", "99.9"))
        .andReturn()
        .getResponse();
    assertEquals("ResponseDto{requestId='main', data=null, "
            + "error=ResponseMessageDto{code='err-r-13', "
            + "message='Parameter \"id\" type mismatch.', "
            + "type=ERROR, "
            + "helpUri='null', "
            + "context=[ContextInfoDto{"
            + "name='id', value=99.9, valueType='null', help=Long, helpType='type', "
            + "message='Required type of the parameter \"id\" is \"Long\". "
            + "However, the value was: \"99.9\"'}]}, messages=[]}",
        response.getContentAsString()
    );
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void handleMissingServletRequestParameterException() throws Exception {
    final MockHttpServletResponse response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.delete("/method/asd"))
        .andReturn()
        .getResponse();
    assertEquals("ResponseDto{requestId='main', data=null, "
            + "error=ResponseMessageDto{code='err-r-6', "
            + "message='Request parameter is missing.', type=ERROR, helpUri='null', "
            + "context=[ContextInfoDto{name='parameter', value=id, valueType='null', "
            + "help=Long, helpType='null', "
            + "message='Parameter \"id\" of type \"Long\" is missing.'}]}, messages=[]}",
        response.getContentAsString());
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void handleTypeMismatchException() throws Exception {
    final MockHttpServletResponse response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.delete("/typeMismatch"))
        .andReturn()
        .getResponse();

    assertEquals("ResponseDto{requestId='main', data=null, "
            + "error=ResponseMessageDto{code='err-r-9', "
            + "message='Property \"id\" type mismatch.', "
            + "type=ERROR, helpUri='null', "
            + "context=[ContextInfoDto{name='id', value=newValue, valueType='null', "
            + "help=Param, helpType='null', "
            + "message='Required type of the property \"id\" is \"Param\". "
            + "However, the value was: \"newValue\"'}]}, messages=[]}",
        response.getContentAsString()
    );
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void httpRequestMethodNotSupported() throws Exception {
    final MockHttpServletResponse response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.get("/method/asd"))
        .andReturn()
        .getResponse();
    assertEquals("ResponseDto{requestId='main', data=null, "
            + "error=ResponseMessageDto{code='err-r-2', "
            + "message='Http request method \"GET\" not allowed at this endpoint.', "
            + "type=ERROR, helpUri='null', context=[ContextInfoDto{name='method', "
            + "value=GET, valueType='string', help=[DELETE, POST], helpType='set', "
            + "message='Supported methods are: [DELETE, POST].'}]}, messages=[]}",
        response.getContentAsString()
    );
    assertEquals(HttpStatus.METHOD_NOT_ALLOWED.value(), response.getStatus());
  }

  @Test
  void testMissingServletRequestPartException() throws Exception {

    MockMultipartFile file = new MockMultipartFile("filea", "hello.txt",
        MediaType.TEXT_PLAIN_VALUE, "Hello, World!".getBytes());
    final String response = customFilterMockMvc
        .perform(MockMvcRequestBuilders.multipart("/multipart").file(file))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
        + "code='err-r-12', message='Part of a \"multipart/form-data\" request "
        + "identified by its name \"file\" cannot be found.', type=ERROR, helpUri='null', "
        + "context=[ContextInfoDto{name='requestPartName', value=file, valueType='null', "
        + "help=null, helpType='null', message='null'}]}, messages=[]}", response);
  }

  @SuppressWarnings("EmptyMethod")
  void handle(final ContextInfoDto arg) {
    //testing method
  }

  @RestController
  private static class TestingController {


    @PostMapping("/multipart")
    public ResponseEntity<String> handleMultipart(@RequestParam("file") final MultipartFile file) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @DeleteMapping("/method/{param}")
    public ResponseEntity<String> handleDeleteRequest(@PathVariable("param") final String param,
        @RequestParam("id") final @Min(100) Long id) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @DeleteMapping("/converter")
    public ResponseEntity<String> handleDeleteRequest(@RequestParam("id") final @Valid Param id) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @GetMapping(value = "/mediaTypeNotAcceptable", produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> handleMediaTypeNotAcceptableRequest() {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @GetMapping(value = "/mediaType", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> handleMediaTypeRequest() {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @PostMapping("/method/{param}")
    public ResponseEntity<String> handlePatchRequest(@PathVariable("param") final String param,
        @RequestParam("id") final Long id) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @GetMapping("/missing-path-variable/{id}/{param}")
    public ResponseEntity<String> missingPathVariableException(@PathVariable("param") final String param,
        @PathVariable("id") final Long id) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @GetMapping("/servlet-request-binding-exception")
    public ResponseEntity<String> servletRequestBindingException(@RequestHeader("param") final String param) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @PostMapping(
        value = "/http-message-not-readable-exception",
        produces = MediaType.TEXT_HTML_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<HttpNotRwObject> httpMessageNotReadableException(@RequestBody final HttpNotRwObject body) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @GetMapping(value = "/http-message-not-writable-exception", produces = MediaType.APPLICATION_GRAPHQL_RESPONSE_VALUE)
    public ResponseEntity<HttpNotRwObject> httpMessageNotWritableException() {
      return ResponseEntity.ok(new HttpNotRwObject());
    }

    @GetMapping("/throwNotAliveExceptions")
    public ResponseEntity<String> handleThrowStaticMocked(@RequestParam("name") final String name) {
      switch (name) {
        case "AsyncRequestTimeoutException":
          throw new AsyncRequestTimeoutException();
        case "HttpMessageNotWritableException":
          throw new HttpMessageNotWritableException("");
        case "HttpMessageNotReadableException":
          throw new HttpMessageNotReadableException("message", new MockHttpInputMessage(new byte[0]));
        default:
          throw new UnsupportedOperationException("Invalid call.");
      }
    }

    @DeleteMapping("/typeMismatch")
    public ResponseEntity<String> handleTypeMismatch() {

      throw new TypeMismatchException(
          new PropertyChangeEvent(new Param(), "id", "oldValue", "newValue"),
          Param.class);
    }
  }

  @ControllerAdvice
  private static class TestSpringWebRequestExceptionResolverControllerAdvice implements
      SpringWebRequestExceptionResolver<String> {

    private final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    private final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    final ProcessingIdProvider pid = () -> "main";
    private final ThreadLocalStorage storage = new ThreadLocalStorage(pid);
    private final BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle(RESOURCE_BUNDLE_NAME, "i18n.errors-test"))
        .withMessageStorage(storage)
        .withProcessingIdProvider(pid)
        .withConfig(this.config)
        .withLogMapper(new MessageTypeLogMapper(this.logSanitizer, this.config))
        .withExposeSanitizer(new DefaultSecurityInfoSanitizer(
            ProcessingExceptionConfig::defaultSecurityPolicy,
            ProcessingExceptionConfig::exposeSecurityPolicies))
        .build();

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return this.mapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return this.mapper.getLogMapper();
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }
  }

  private static class Param {

    private Long id;
  }
}
