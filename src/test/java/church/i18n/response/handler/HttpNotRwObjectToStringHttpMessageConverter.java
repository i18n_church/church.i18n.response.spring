/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import java.nio.charset.StandardCharsets;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

public class HttpNotRwObjectToStringHttpMessageConverter extends
    AbstractHttpMessageConverter<HttpNotRwObject> {

  public HttpNotRwObjectToStringHttpMessageConverter() {
    super(StandardCharsets.UTF_8, MediaType.APPLICATION_JSON);
  }

  @Override
  protected boolean supports(final Class<?> clazz) {
    return HttpNotRwObject.class == clazz;
  }

  @Override
  protected HttpNotRwObject readInternal(
      final Class<? extends HttpNotRwObject> clazz,
      final HttpInputMessage inputMessage) throws HttpMessageNotReadableException {
    throw new HttpMessageNotReadableException("Manual exception: ResponseDto deserialization issue", inputMessage);
  }

  @Override
  protected void writeInternal(final HttpNotRwObject t, final HttpOutputMessage outputMessage)
      throws HttpMessageNotWritableException {
    throw new HttpMessageNotWritableException("Manual exception: ResponseDto deserialization issue");
  }
}
