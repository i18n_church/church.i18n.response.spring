/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.ResponseMapper;
import jakarta.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

class BaseHandlerTest {

  @ParameterizedTest(name = "{index}. Method: {0}")
  @MethodSource("getValidControllers")
  void allHandlersMethods_annotation_is_present(final Method method) {
    //annotation present
    assertTrue(method.isAnnotationPresent(ExceptionHandler.class));
  }

  @ParameterizedTest(name = "{index}. Method: {0}")
  @MethodSource("getValidControllers")
  void allHandlersMethods_method_and_annotation_exception_types_match(final Method method) {
    ExceptionHandler annotation = method.getAnnotation(ExceptionHandler.class);
    Class<?> methodExceptionType = method.getParameterTypes()[0];
    //all values from the annotation is possible to assign into method (method will correctly handle that)
    Arrays.stream(annotation.value()).forEach(
        annotationException -> assertTrue(methodExceptionType.isAssignableFrom(annotationException),
            () -> MessageFormat.format("Method {0} contains exception handling mismatch. " +
                    "Method Exception Type: {1} is not assignable from type specified in annotation: {2}",
                method.getName(), methodExceptionType.getCanonicalName(),
                annotationException.getCanonicalName()
            ))
    );
  }

  @ParameterizedTest(name = "{index}. Method: {0}")
  @MethodSource("getInvalidControllers")
  void allHandlersMethods_method_and_annotation_exception_types_not_match(final Method method) {
    ExceptionHandler annotation = method.getAnnotation(ExceptionHandler.class);
    Class<?> methodExceptionType = method.getParameterTypes()[0];
    //all values from the annotation is possible to assign into method (method will correctly handle that)
    assertFalse(Arrays.stream(annotation.value()).allMatch(methodExceptionType::isAssignableFrom));
  }

  @ParameterizedTest(name = "{index}. Method: {0}")
  @MethodSource("getValidControllers")
  void allHandlersMethods_method_has_correct_arguments(final Method method) {
    assertEquals(3, method.getParameterCount());
    Class<?>[] parameterTypes = method.getParameterTypes();
    assertTrue(Throwable.class.isAssignableFrom(parameterTypes[0]));
    assertTrue(HttpServletRequest.class.isAssignableFrom(parameterTypes[1]));
    assertTrue(WebRequest.class.isAssignableFrom(parameterTypes[2]));
    //annotation handler and

  }

  private static Stream<Arguments> getMethods(final Class<?>... clazz) {
    return Arrays.stream(clazz)
        .flatMap(c -> Arrays.stream(c.getDeclaredMethods()))
        .filter(m -> ResponseEntity.class.isAssignableFrom(m.getReturnType()))
        .map(Arguments::of);
  }

  private static Stream<Arguments> getInvalidControllers() {
    return getMethods(TestAnnotationsNotMatchControllerAdvice.class);
  }

  private static Stream<Arguments> getValidControllers() {
    //one day, it might be classpath scan
    Class<?>[] handlers = new Class[]{
        GenericExceptionHandler.class,
        ProcessingExceptionHandler.class,
        SpringWebRequestExceptionResolver.class,
        //mocked controllers
        TestAnnotationsMatchControllerAdvice.class
    };
    return getMethods(handlers);
  }

  @ControllerAdvice
  private static class TestAnnotationsMatchControllerAdvice implements
      GenericExceptionHandler<String> {

    final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    private final ThreadLocalStorage storage = new ThreadLocalStorage(() -> "static-processing-id");

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return null;
    }

    @Override
    public LogMapper getLogMapper() {
      return new MessageTypeLogMapper(logSanitizer, config);
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }

    @ExceptionHandler({ArrayIndexOutOfBoundsException.class, StringIndexOutOfBoundsException.class})
    ResponseEntity<String> a(final IndexOutOfBoundsException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @ExceptionHandler({IllegalThreadStateException.class, NumberFormatException.class,
        IllegalArgumentException.class})
    ResponseEntity<String> a(final IllegalArgumentException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    @ExceptionHandler({IllegalThreadStateException.class, NumberFormatException.class,
        IllegalArgumentException.class, ArrayIndexOutOfBoundsException.class,
        StringIndexOutOfBoundsException.class})
    ResponseEntity<String> a(final RuntimeException e, final HttpServletRequest servletRequest,
        final WebRequest request) {
      throw new UnsupportedOperationException("Invalid call.");
    }
  }

  @ControllerAdvice
  private static class TestAnnotationsNotMatchControllerAdvice implements
      GenericExceptionHandler<String> {

    final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    private final ThreadLocalStorage storage = new ThreadLocalStorage(() -> "static-processing-id");

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return null;
    }

    @Override
    public LogMapper getLogMapper() {
      return new MessageTypeLogMapper(logSanitizer, config);
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }

    //super class specified
    @ExceptionHandler(IndexOutOfBoundsException.class)
    ResponseEntity<String> a(final ArrayIndexOutOfBoundsException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    //different exception
    @ExceptionHandler({ArithmeticException.class, ArrayStoreException.class})
    ResponseEntity<String> handleIllegalCallerException(final ClassCastException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      throw new UnsupportedOperationException("Invalid call.");
    }

    //the same level, different exception
    @ExceptionHandler({IllegalThreadStateException.class, NumberFormatException.class,
        IllegalArgumentException.class})
    ResponseEntity<String> handleIllegalCallerException(final NullPointerException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      throw new UnsupportedOperationException("Invalid call.");
    }
  }
}
