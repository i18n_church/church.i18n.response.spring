/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.status.ClientError;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import java.util.FormatFlagsConversionMismatchException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

class ProcessingExceptionHandlerTest {

  private static final ThreadLocalStorage threadLocalStorage = new ThreadLocalStorage(() -> "main");

  private static final MockMvc mvcMock = MockMvcBuilders
      .standaloneSetup(new ExceptionController())
      .setControllerAdvice(new TestProcessingExceptionHandler())
      .setMessageConverters(new ResponseDtoToStringHttpMessageConverter())
      //.addDispatcherServletCustomizer(servlet -> servlet.setThrowExceptionIfNoHandlerFound(true))
      .build();

  @Test
  void controllersServiceDefaultRemapping() throws Exception {
    final MockHttpServletResponse servletResponse = mvcMock
        .perform(MockMvcRequestBuilders.get("/default-remapping"))
        .andReturn()
        .getResponse();
    assertEquals(500, servletResponse.getStatus());
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
        + "code='err-g-1', message='Internal Server Error.', type=ERROR, helpUri='null', "
        + "context=[]}, messages=[]}", servletResponse.getContentAsString());
  }

  @Test
  void controllersServiceThrowsException() throws Exception {
    final MockHttpServletResponse servletResponse = mvcMock
        .perform(MockMvcRequestBuilders.get("/"))
        .andReturn()
        .getResponse();
    assertEquals(415, servletResponse.getStatus());
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{code='code', "
        + "message='Code value param1 and param2.', type=ERROR, helpUri='null', "
        + "context=[ContextInfoDto{name='name', value=context, valueType='contextType', "
        + "help=help, helpType='helpType', message='Test Error Message w/ double param: 3.3.'}, "
        + "ContextInfoDto{name='name2', value=context2, valueType='contextType2', help=format2, "
        + "helpType='formatType2', message='Test Error Message2 w/ double param: {k1=v1, k2=v2}."
        + "'}]}, messages=[]}", servletResponse.getContentAsString());
  }

  @RestController
  private static class ExceptionController {

    private final ExceptionService exceptionService = new ExceptionService();

    @GetMapping("/default-remapping")
    @ResponseBody
    public ResponseEntity<String> defaultRemapping() {
      //when no http status is specified, it is mapped to internal server error by default.
      throw new ProcessingException("err-a-1").withMessageType(MessageStatus.INFO);
    }

    @GetMapping("/")
    @ResponseBody
    public ResponseEntity<String> handleRequest() {
      this.exceptionService.throwI18nException();
      return new ResponseEntity<>("Fail", HttpStatus.NOT_ACCEPTABLE);
    }
  }

  @ControllerAdvice
  private static class TestProcessingExceptionHandler implements
      ProcessingExceptionHandler<Response<?>> {

    final ProcessingIdProvider pid = () -> "main";
    private final ThreadLocalStorage storage = new ThreadLocalStorage(pid);
    final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    final BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle("i18n.errors-test"))
        .withProcessingIdProvider(pid)
        .withMessageStorage(ProcessingExceptionHandlerTest.threadLocalStorage)
        .withConfig(this.config)
        .withLogMapper(new MessageTypeLogMapper(this.logSanitizer, this.config))
        .withExposeSanitizer(new DefaultSecurityInfoSanitizer(
            ProcessingExceptionConfig::defaultSecurityPolicy,
            ProcessingExceptionConfig::exposeSecurityPolicies))
        .build();

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return this.mapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }

  }

  private static class ExceptionService {

    void throwI18nException() {
      Exception origin = new FormatFlagsConversionMismatchException("Exception origin 1", 'W');
      Exception origin2 = new IllegalArgumentException("Exception origin 2");

      ProcessingExceptionHandlerTest.threadLocalStorage
          .addMessages("", new ProcessingMessage("code-2", "paramX", "paramY", origin2));
      throw new ProcessingException("code", "param1", "param2", origin)
          .withStatus(ClientError.UNSUPPORTED_MEDIA_TYPE)
          .addContextInfo(
              ContextInfo.of("name")
                  .withContext("context", "contextType")
                  .withHelp("help", "helpType")
                  .withMessage("err-ctx-t-2", 3.3d)
                  .build()
          )
          .addContextInfo(
              ContextInfo.of("name2")
                  .withContext("context2", "contextType2")
                  .withHelp("format2", "formatType2")
                  .withMessage("err-ctx-t-3", new TreeMap<>(Map.of("k1", "v1", "k2", "v2")))
                  .build()
          );
    }
  }

}
