/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import church.i18n.response.domain.ResponseDto;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.StreamUtils;

public class ResponseDtoToStringHttpMessageConverter extends
    AbstractHttpMessageConverter<ResponseDto<String>> {

  public ResponseDtoToStringHttpMessageConverter() {
    super(StandardCharsets.UTF_8, MediaType.TEXT_HTML);
  }

  @Override
  protected boolean supports(final Class<?> clazz) {
    return ResponseDto.class == clazz;
  }

  @Override
  protected ResponseDto<String> readInternal(
      final Class<? extends ResponseDto<String>> clazz,
      final HttpInputMessage inputMessage) throws HttpMessageNotReadableException {
    throw new UnsupportedOperationException("Not needed");
  }

  @Override
  protected void writeInternal(final ResponseDto<String> t,
      final HttpOutputMessage outputMessage)
      throws IOException, HttpMessageNotWritableException {
    StreamUtils.copy(restResponseDtoToString(t), StandardCharsets.UTF_8, outputMessage.getBody());
  }

  private String restResponseDtoToString(final ResponseDto<String> o) {
    return o.toString();
  }
}
