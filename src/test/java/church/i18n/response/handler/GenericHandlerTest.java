/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.handler;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.status.ClientError;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.mapper.ResponseMapper;
import church.i18n.response.spring.mapper.ResponseEntityExceptionMapper;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.support.StaticWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

class GenericHandlerTest {

  private static final Object[] EMPTY_PARAMS = new Object[0];

  private static final MockMvc mvcMock = MockMvcBuilders
      .standaloneSetup(new TestingController())
      .setControllerAdvice(new TestRequestHandlerControllerAdvice())
      .setMessageConverters(new ResponseDtoToStringHttpMessageConverter(),
          new StringHttpMessageConverter())
      //.addDispatcherServletCustomizer(servlet -> servlet.setThrowExceptionIfNoHandlerFound(true))
      .build();

  @ParameterizedTest
  @MethodSource("testEndpoints")
  void testEndpoints(final String endpoint, final String response, final int httpStatus)
      throws Exception {
    final MockHttpServletResponse servletResponse = mvcMock
        .perform(MockMvcRequestBuilders.get(endpoint))
        .andReturn()
        .getResponse();
    assertEquals(servletResponse.getContentAsString(), response);
    assertEquals(httpStatus, servletResponse.getStatus());
  }

  @ParameterizedTest
  @MethodSource("testEndpointsLocalization")
  void testEndpointsLocalization(final String endpoint, final Locale[] localizationInfo,
      final String response, final int httpStatus) throws Exception {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE); //zh_CN

      ServletContext servletCtx = new MockServletContext();
      servletCtx.setAttribute("x", "a");
      MockHttpServletRequest servletRequest = new MockHttpServletRequest(servletCtx, "GET",
          endpoint);
//            servletRequest.addHeader("Accept-Language", localizationInfo);
      servletRequest.setPreferredLocales(Arrays.asList(localizationInfo));
      MockHttpServletResponse servletResponse = new MockHttpServletResponse();
      servletResponse.setCharacterEncoding("UTF-8");
      servletResponse.setContentType("text/plain");

      StaticWebApplicationContext ctx = new StaticWebApplicationContext();
      ctx.registerSingleton("controller", GenericHandlerTest.TestingController.class);
      ctx.registerSingleton("exceptionHandler",
          GenericHandlerTest.TestRequestHandlerLocalizedControllerAdvice.class);

      ctx.refresh();
      DispatcherServlet servlet = new DispatcherServlet(ctx);
      servlet.init(new MockServletConfig());
      //servlet.setThrowExceptionIfNoHandlerFound(true);
      servlet.service(servletRequest, servletResponse);

      assertEquals(response, servletResponse.getContentAsString());
      assertEquals(httpStatus, servletResponse.getStatus());


    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  private static Stream<Arguments> testEndpoints() {
    return Stream.of(
        Arguments.of("/cloneNotSupported", "err-g-1", 500),
        Arguments.of("/illegalCallerException", "i-am-a-teapot-code", 410)
    );
  }

  private static Stream<Arguments> testEndpointsLocalization() {
    //"en_GB, en_US;q=0.8, zh_CN;q=0.7, fr_FR;q=0.3, zh_TW;q=0.9, en;q=0.2, *;q=0.5";
    // @formatter:off
    Locale en = Locale.ENGLISH;
    Locale de = Locale.forLanguageTag("de");
    Locale enGB = Locale.forLanguageTag("en-GB");
    Locale enUs = Locale.forLanguageTag("en-US");
    Locale enIe = Locale.forLanguageTag("en-IE");
    Locale zhCn = Locale.forLanguageTag("zh-CN");
    Locale zhTw = Locale.forLanguageTag("zh-TW");
    Locale frBe = Locale.forLanguageTag("fr-BE");
    Locale noNo = Locale.forLanguageTag("no-NO");

    return Stream.of(
        Arguments.of("/noSuchFieldException", new Locale[]{enIe, frBe, noNo}, "套间", 410),
//when non-existing language, fail to country code
        Arguments.of("/noSuchFieldException", new Locale[]{de, zhTw, zhCn}, "套間", 410),
//fail over non-existing
        Arguments.of("/illegalCallerException", new Locale[]{en}, "i-am-a-teapot-code", 409),
        Arguments.of("/noSuchFieldException", new Locale[]{de}, "套间", 410),
        Arguments.of("/noSuchFieldException", new Locale[]{enGB, enUs, zhCn}, "flat", 410)
    );
    // @formatter:on
  }

  @RestController
  private static class TestingController {

    @GetMapping("/cloneNotSupported")
    public ResponseEntity<String> handleCloneNotSupported() throws Exception {
      throw new java.lang.CloneNotSupportedException();
    }

    @GetMapping("/illegalCallerException")
    public ResponseEntity<String> handleIllegalCallerException() {
      throw new java.lang.IllegalCallerException();
    }

    @GetMapping(value = "/noSuchFieldException", produces = "text/plain; charset=UTF-8")
    public ResponseEntity<String> handleNoSuchFieldException() throws NoSuchFieldException {
      //test localization message not found
      throw new java.lang.NoSuchFieldException();
    }
  }

  @ControllerAdvice
  private static class TestRequestHandlerControllerAdvice implements
      ResponseEntityExceptionMapper<String>, GenericExceptionHandler<String> {

    private final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    private final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    private static final ResponseMapper<String> stringCodeMapper = new ResponseMapper<>() {
      @Override
      public @NotNull <T> String map(final @Nullable T data,
          final @Nullable ProcessingMessage error,
          final @Nullable List<Locale> locales, final @Nullable ProcessingMessage... messages) {
        assert error != null;
        return error.getMessage().getCode();
      }
    };
    private final ThreadLocalStorage storage = new ThreadLocalStorage(() -> "static-processing-id");

    @Override
    public ResponseMapper<String> getMapper() {
      return stringCodeMapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return new MessageTypeLogMapper(this.logSanitizer, this.config);
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }

    @ExceptionHandler(IllegalCallerException.class)
    ResponseEntity<String> handleIllegalCallerException(final IllegalCallerException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      ProcessingException exception = new ProcessingException("i-am-a-teapot-code", e)
          .withStatus(ClientError.GONE);
      return error(exception);
    }
  }

  @ControllerAdvice
  private static class TestRequestHandlerLocalizedControllerAdvice implements
      GenericExceptionHandler<String> {

    private final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    private final SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies);
    private final ThreadLocalStorage storage = new ThreadLocalStorage(() -> "static-processing-id");

    private final ResponseMapper<String> stringCodeMapper = new ResponseMapper<>() {
      private final PolyglotMultiResourceBundle messages = new PolyglotMultiResourceBundle(
          "i18n.A");

      @Override
      public @NotNull <T> String map(final @Nullable T data,
          final @Nullable ProcessingMessage error,
          final @Nullable List<Locale> locales, final @Nullable ProcessingMessage... messages) {
        String code = error.getMessage().getCode();
        MessageFormat messageFormat = this.messages.getString(code, locales)
            .orElse(new MessageFormat(code));
        return messageFormat.format(GenericHandlerTest.EMPTY_PARAMS);
      }
    };

    @Override
    public ResponseMapper<String> getMapper() {
      return this.stringCodeMapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return new MessageTypeLogMapper(this.logSanitizer, this.config);
    }

    @Override
    public MessageStorage getMessageStorage() {
      return this.storage;
    }

    @ExceptionHandler(NoSuchFieldException.class)
    ResponseEntity<String> handleIllegalCallerException(final NoSuchFieldException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      ProcessingException exception = new ProcessingException("msg-A1", e)
          .withStatus(ClientError.GONE);
      return error(exception, Collections.list(servletRequest.getLocales()));
    }

    @ExceptionHandler(IllegalCallerException.class)
    ResponseEntity<String> handleIllegalCallerException(final IllegalCallerException e,
        final HttpServletRequest servletRequest, final WebRequest request) {
      ProcessingException exception = new ProcessingException("i-am-a-teapot-code", e)
          .withStatus(ClientError.CONFLICT);
      return error(exception, Collections.list(servletRequest.getLocales()));
    }
  }
}
