/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.request.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

class HttpHeaderRequestIdThreadNameFilterTest {

  private final MockMvc mockMvc = MockMvcBuilders
      .standaloneSetup(new MockRequestUuidController())
      .addFilter(new HttpHeaderRequestIdThreadNameFilter())
      .build();

  @Test
  void noHeader_generatesUniqueUuid() throws Exception {
    final int requests = 100;
    final List<String> uuids = new ArrayList<>(requests);
    for (int i = 0; i < requests; i++) {
      final String uuid = mockMvc
          .perform(MockMvcRequestBuilders.get("/getThreadName"))
          .andReturn()
          .getResponse()
          .getContentAsString();
      assertUuid(uuid);
      uuids.add(uuid);
    }
    final List<String> uniqueUuids = new ArrayList<>(Set.copyOf(uuids));
    Collections.sort(uuids);
    Collections.sort(uniqueUuids);
    assertEquals(uuids, uniqueUuids, "Filter generated non-unique UUID.");
  }

  @Test
  void testEmptyUuid() throws Exception {
    final String uuid = mockMvc
        .perform(MockMvcRequestBuilders.get("/getThreadName")
            .header("X-Request-ID", ""))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertUuid(uuid);
  }

  @Test
  void testMissingHeaderUuid() throws Exception {
    final String uuid = mockMvc
        .perform(MockMvcRequestBuilders.get("/getThreadName"))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertUuid(uuid);
  }

  @Test
  void testCustomUuidSettings_withHeader() throws Exception {

    final MockMvc customFilterMockMvc = MockMvcBuilders
        .standaloneSetup(new MockRequestUuidController())
        .addFilter(new HttpHeaderRequestIdThreadNameFilter("custom-header", 10, "[^a]", "?"))
        .build();

    final String uuid = customFilterMockMvc
        .perform(MockMvcRequestBuilders.get("/getThreadName")
            .header("custom-header", "?a??a??a??a??a??"))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertEquals("?a??a??a??", uuid);
  }

  @Test
  void testInvalidConstructorMaxLenghtSetting() {
    assertThrows(IllegalArgumentException.class,
        () -> new HttpHeaderRequestIdThreadNameFilter("name", 0, "", ""));
  }

  @Test
  void testCustomUuidSettings_differentHeader() throws Exception {
    final MockMvc customFilterMockMvc = MockMvcBuilders
        .standaloneSetup(new MockRequestUuidController())
        .addFilter(new HttpHeaderRequestIdThreadNameFilter("expected-header", 10, "[^a]", "?"))
        .build();

    final String uuid = customFilterMockMvc
        .perform(MockMvcRequestBuilders.get("/getThreadName")
            .header("custom-header", "invalid-uuid"))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertUuid(uuid);
  }

  @Test
  void testCustomUuidSettings_noHeader() throws Exception {

    final MockMvc customFilterMockMvc = MockMvcBuilders
        .standaloneSetup(new MockRequestUuidController())
        .addFilter(new HttpHeaderRequestIdThreadNameFilter("custom-header", 10, "[a]", "?"))
        .build();

    final String uuid = customFilterMockMvc
        .perform(MockMvcRequestBuilders.get("/getThreadName"))
        .andReturn()
        .getResponse()
        .getContentAsString();
    assertUuid(uuid);
  }

  /**
   * 1. valid header with value 2. invalid header name 3. length (200) 4. sanitization (regexp+repl
   * string)
   */
  @ParameterizedTest
  @MethodSource("requestUuids")
  void requestUuid_isSet(final String requestUuid, final String expectedUuid) throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
        .get("/getThreadName")
        .header("X-Request-ID", requestUuid))
        .andExpect(content().string(expectedUuid));
  }

  private static Stream<Arguments> requestUuids() {
    return Stream.of(
        Arguments.of("a", "a"),
        Arguments.of("bla-bla", "bla-bla"),
        Arguments.of("dK0YHekTY4zZU3agqnXWclvaboDfyhAKsysy11SMT0C7MzXM3ZFZUcpOP"
                + "IqQlQxSTEH0ei0i7iFtGwdR6243zDDC3rFZuivfSwKGKJnpJ7OmegiLQ6fMP"
                + "DTZ3h1JX1BOBEj0OXY8Pv527Fm9aSEMlJ9NpdbFUcUS9r93HXWxssYazoqDe"
                + "ARi9MYuBLGsVpxJbIMw9YJO m5XXXcnJuRC2BZkyCWns",
            "dK0YHekTY4zZU3agqnXWclvaboDfyhAKsysy11SMT0C7MzXM3ZFZUcpOPIqQlQxSTE"
                + "H0ei0i7iFtGwdR6243zDDC3rFZuivfSwKGKJnpJ7OmegiLQ6fMPDTZ3h1JX1"
                + "BOBEj0OXY8Pv527Fm9aSEMlJ9NpdbFUcUS9r93HXWxssYazoqDeARi9MYuBL"
                + "GsVpxJbIMw9YJO"),
        Arguments.of("_-+=,.;/", "_-+=,.;/"),
        Arguments.of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"),
        Arguments.of("0123456789", "0123456789"),
        Arguments.of("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{¦}~",
            "+++++++++++,-./+;+=+++++++_+++++"),
        Arguments.of("ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜ",
            "+++++++++++++++++++++++++++"),
        Arguments.of("¢£¥Pt�áíóúñÑª°¿¬½¼¡«»ßµø±÷°·.²",
            "+++Pt+++++++++++++++++++++++.+"),
        Arguments.of("~æøåÜäößöäüöüßäΓαζέεςκαὶμυρτιὲ", "++++++++++++++++++++++++++++++"),
        Arguments.of("ςδὲνθὰβρῶπιὰστὸχρυσαφὶξέφωτοΞε", "++++++++++++++++++++++++++++++"),
        Arguments.of("σκεπάζωτὴνψυχοφθόραβδελυγμίαüó", "++++++++++++++++++++++++++++++"),
        Arguments.of("íñîéàôé'ôïùû'âàæè'êëàÿüçààéœ'î", "++++++++++++++++++++++++++++++"),
        Arguments.of("ëù'èûê'ïüÂéÔœéçœéç'âôïÿêüëàîèä", "++++++++++++++++++++++++++++++"),
        Arguments.of("öùûæ'ÍÚÓóÉÁÁíűőüöúóéæýöéþóúæðí", "++++++++++++++++++++++++++++++"),
        Arguments.of("áæöéáðþíúóýいろはにほへとちりぬるをわかよたれそつ", "++++++++++++++++++++++++++++++"),
        Arguments.of("ねならむうゐのおくやまけふこえてあさきゆめみしゑひもせすイロ", "++++++++++++++++++++++++++++++"),
        Arguments.of("ハニホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフ", "++++++++++++++++++++++++++++++"),
        Arguments.of("コエテアサキユメミシヱヒモセスンדגסקרןשטביםמאו", "++++++++++++++++++++++++++++++"),
        Arguments.of("כזבולפתעמצאלוחברהאיךהקליטהąćęł", "++++++++++++++++++++++++++++++"),
        Arguments.of("óźżśńВчащахюгажилбыцитрусДаноф", "++++++++++++++++++++++++++++++"),
        Arguments.of("альшивыйэкземпляр!!Съешьжеещёэ", "++++++++++++++++++++++++++++++"),
        Arguments.of("тихмягкихфранцузскихбулокдавып", "++++++++++++++++++++++++++++++"),
        Arguments.of("ейчаю๏เป็นมนุษย์สุดประเสริฐเลิ", "++++++++++++++++++++++++++++++"),
        Arguments.of("ศคุณค่ากว่าบรรดาฝูงสัตว์เดรัจฉ", "++++++++++++++++++++++++++++++"),
        Arguments.of("านจงฝ่าฟันพัฒนาวิชาการอย่าล้าง", "++++++++++++++++++++++++++++++"),
        Arguments.of("ผลาญฤๅเข่นฆ่าบีฑาใครไม่ถือโทษโ", "++++++++++++++++++++++++++++++"),
        Arguments.of("กรธแช่งซัดฮึดฮัดด่าหัดอภัยเหมื", "++++++++++++++++++++++++++++++"),
        Arguments.of("อนกีฬาอัชฌาสัยปฏิบัติประพฤติกฎ", "++++++++++++++++++++++++++++++"),
        Arguments.of("กำหนดใจพูดจาให้จ๊ะๆจ๋าๆน่าฟังเ", "++++++++++++++++++++++++++++++"),
        Arguments.of("อยฯığışöçü", "++++++++++")
    );
  }

  private void assertUuid(final String uuid) {
    try {
      UUID.fromString(uuid);
    } catch (IllegalArgumentException ex) {
      fail(uuid + " is not valid UUID.");
    }
  }

  @RestController
  private static class MockRequestUuidController {

    @GetMapping("/getThreadName")
    String testRequestUuid() {
      return Thread.currentThread().getName();
    }

  }
}
