# Spring REST error handling

The library extends `BaseResponseMapper` from the
[church.i18n.response](https://bitbucket.org/i18n_church/church.i18n.response)
project and adds support for spring-web applications. The library defines:

* **Mapper**: `BaseHttpResponseMapper` for easier and direct manipulation with responses, exceptions
  and `MessageStorage`
* **Exception Handlers**: Generic, `ProcessingException` and Spring-web related exceptions.
* **Filters**: Filters handling (and serializing with configured serializers) `Exception` and
  `ProcessingException` exceptions when they occur in a Spring filters chain (a default Spring
  behavior is returning an HTML page with an error, which breaks a desired common response
  structure).

## Mapper

## Exception handlers

### Extending default Exception Handlers

A particular set of exceptions is already covered by default exception handlers. All you need to do
is implement those handlers in your `@ControllerAdvice`.

```java

@ControllerAdvice
public class ExceptionControllerAdvice implements
    GenericHandler<Response<Object>>,                     //Exception.class handler
    ProcessingExceptionHandler<Response<Object>>,         //ProcessingException.class handler
    SpringWebRequestExceptionResolver<Response<Object>>,  //Spring-web specific exceptions
    WebRequestHandler<Response<Object>> {

  BaseHttpResponseMapper mapper = new BaseHttpResponseMapper(
      GenericHandler.RESOURCE_BUNDLE_NAME,
      ProcessingExceptionHandler.RESOURCE_BUNDLE_NAME, 
      SpringWebRequestExceptionResolver.RESOURCE_BUNDLE_NAME,
      "your-custom-resource-bundle");

  @Override
  public HttpResponseMapper<Response<Object>> getMapper() {
    return mapper;
  }

  //your custom exception handlers
  @ExceptionHandler(YourCustomException.class)
  ResponseEntity<R> handleYourCustomException(final YourCustomException exception,
      final HttpServletRequest servletRequest, final WebRequest webRequest) {
    //...code
    ProcessingException yourProcessedException = ...
    return getMapper().map(yourProcessedException, servletRequest, webRequest);
  }

}
```

#### Define a custom handler interface

The definition of your own custom exception handler is similar to when you define it in
`@ControllerAdvice`. The best practise is when you define an exception handler per library, so all
exceptions rising from a library will be encapsulated in a single interface. Moreover, all exception
messages with their codes should be a part of their own resource bundle which name is defined in an
interface as a constant (e.g. `RESOURCE_BUNDLE_NAME`).

```java
public interface CustomExceptionHandler<R> extends WebRequestHandler<R> {

  String RESOURCE_BUNDLE_NAME = "i18n.custom-error-handler-messages";

  @ExceptionHandler(NoHandlerFoundException.class)
  default ResponseEntity<R> handleNoHandlerFoundException(final NoHandlerFoundException exception,
      final HttpServletRequest servletRequest, final WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("custom-error-code", ...)
        .withHttpStatus(ClientError...)
        .with(...)
        .addContextInfo(...)
        .build();
    return getMapper().map(processingException, servletRequest, webRequest);
  }
}
```

### Default exception handlers

**SpringWebRequestExceptionResolver.class**
The most common Spring WebMVC Exceptions

* AsyncRequestTimeoutException.class
* ConversionNotSupportedException.class
* HttpMediaTypeNotAcceptableException.class
* HttpMediaTypeNotSupportedException.class
* HttpMessageNotReadableException.class
* HttpMessageNotWritableException.class
* HttpRequestMethodNotSupportedException.class
* MethodArgumentTypeMismatchException.class
* MissingPathVariableException.class
* MissingServletRequestParameterException.class
* MissingServletRequestPartException.class
* ServletRequestBindingException.class
* TypeMismatchException.class

**ProcessingExceptionHandler.class**
handles ProcessingException

* ProcessingException.class

**GenericHandler.class**
Any non-handled exception

* Exception.class

## Filters

Definition of an exception handler with defined message converters in spring boot application:

```java
@Configuration
public class AppConfig extends WebMvcConfigurationSupport {

  @Bean
  public FilterRegistrationBean<GenericExceptionHandlerFilter> genericExceptionFilter() {
    FilterRegistrationBean<GenericExceptionHandlerFilter> registration = new FilterRegistrationBean<>();
    GenericExceptionHandlerFilter genericExceptionHandlerFilter = 
        new GenericExceptionHandlerFilter(httpResponseMapper(), getMessageConverters());
    registration.setFilter(genericExceptionHandlerFilter);
    registration.setOrder(Integer.MIN_VALUE);
    return registration;
  }
  
  @Bean
  public FilterRegistrationBean<I18nResponseExceptionFilter> i18nResponseExceptionFilter() {
    FilterRegistrationBean<ProcessingExceptionFilter> registration = new FilterRegistrationBean<>();
    ProcessingExceptionFilter processingExceptionFilter = 
        new ProcessingExceptionFilter(httpResponseMapper(), getMessageConverters());
    registration.setFilter(processingExceptionFilter);
    registration.setOrder(Integer.MIN_VALUE + 1);
    return registration;
  }
}
```

This will cause that even in the case there is an unexpected exception thrown in any filter (with a
higher order), it will be handled by default exception handlers and will ensure the response from
the application will always be in a desired response structure.
